import { getBlockedUrls, addBlockedUrl, replaceBlockedUrls } from './storage';


// Fill table about currently blocked URLs
async function populateTable() {
    const tbody = document.getElementById("blocklist-table-tbody") as HTMLTableSectionElement;

    const blockedUrls = await getBlockedUrls();

    blockedUrls.forEach((url, index) => {
        const row = tbody.insertRow();

        // add unblock button
        const buttonCell = row.insertCell();
        const button = document.createElement("button");
        buttonCell.appendChild(button);
        button.appendChild(document.createTextNode("Unblock"));
        button.onclick = async () => {
            blockedUrls.splice(index, 1);
            replaceBlockedUrls(blockedUrls);
            tbody.removeChild(row);
        }

        // add info on blocked url
        const urlCell = row.insertCell();
        urlCell.appendChild(document.createTextNode(url["url"]));
        const fromCell = row.insertCell();
        fromCell.appendChild(document.createTextNode(composeTimeString(url["from"])));
        const toCell = row.insertCell();
        toCell.appendChild(document.createTextNode(composeTimeString(url["to"])));
    });
}

// Parse time string in format "hh:mm" to integer array [hh, mm]
function parseTimeString(timeString: string): [number, number] {
    const nums = timeString.split(":");
    return [Number(nums[0]), Number(nums[1])];
}

// From integer array [hh, mm] compose time string in format "hh:mm"
function composeTimeString(intArray: number[]) {
    const hours = String(intArray[0]).padStart(2, "0");
    const minutes = String(intArray[1]).padStart(2, "0");
    return `${hours}:${minutes}`;
}

// Add Logic for form to add blocked URL
async function addFormLogic() {
    const form = document.getElementById("block-form");

    const urlInput = document.getElementById("url") as HTMLInputElement;
    const fromInput = document.getElementById("from-time") as HTMLInputElement;
    const toInput = document.getElementById("to-time") as HTMLInputElement;

    form.onsubmit = async (event) => {
        //persist new url
        const submittedUrl = {
            url: urlInput.value,
            from: parseTimeString(fromInput.value),
            to: parseTimeString(toInput.value),
        };

        // check if url is valid
        if (!submittedUrl.url.startsWith("http")) {
            submittedUrl.url = "http://" + submittedUrl.url;
        }
        if (!submittedUrl.url.endsWith("/")) {
            submittedUrl.url = submittedUrl.url + "/";
        }

        try {
            const url = new URL(submittedUrl.url);
        } catch (error) {
            event.preventDefault();
            urlInput.value = "";
            urlInput.placeholder = "INVALID URL";
            return;
        }
        addBlockedUrl(submittedUrl);
    };
}

document.addEventListener("DOMContentLoaded", async function () {
    populateTable();
    addFormLogic();
});