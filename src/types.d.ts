type BlockEntry = {
    url: string
    from: [number, number],
    to: [number, number],
};

interface StoredBlockList extends browser.storage.StorageObject {
    blockedUrls: BlockEntry[]
}
